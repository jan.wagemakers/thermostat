package be.janwagemakers.controlthermostatbluetooth.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Configuration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String airport; // where to get outside information ; EBAW = Belgium Antwerp Airport
    private float  lowTemp; // temperature when nobody is home

    public Configuration() {
        // Default configuration
        this.airport="EBAW";
        this.lowTemp=16.0f;
    }

    public Configuration(String airport, float lowTemp) {
        this.airport=airport;
        this.lowTemp=lowTemp;
    }

    public int getId() {
        return id;
    }

    public String getAirport() {
        return airport;
    }

    public float getLowTemp() {
        return lowTemp;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public void setLowTemp(float lowTemp) {
        this.lowTemp = lowTemp;
    }
}
