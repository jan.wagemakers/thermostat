/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.janwagemakers.controlthermostatbluetooth;

import be.janwagemakers.controlthermostatbluetooth.Entity.Configuration;
import be.janwagemakers.controlthermostatbluetooth.Entity.Rule;
import be.janwagemakers.controlthermostatbluetooth.Entity.User;
import be.janwagemakers.controlthermostatbluetooth.Repository.ConfigurationRepository;
import be.janwagemakers.controlthermostatbluetooth.Repository.RuleRepository;
import be.janwagemakers.controlthermostatbluetooth.Repository.UserRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Calendar;

/**
 * @author jan
 */
@Order(Ordered.LOWEST_PRECEDENCE - 15)
public class StartupRunner implements CommandLineRunner {
    private final Log logger = LogFactory.getLog(getClass());

    private static boolean DEMO = Config.DEMO;

    private static final Serial SERIAL = new Serial();
    private Configuration config;
    private int oldOA = -1;
    private int oldOT = -1;
    private int oldYear = -1;
    private int oldMonth = -1;
    private int oldDay = -1;
    private int oldHour = -1;

    @Autowired
    private ConfigurationRepository configurationRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RuleRepository ruleRepository;
    @Autowired
    private Outside outside;
    @Autowired private DataList data;

    @Override
    public void run(String... args) {
        logger.info("Control Thermostat With Bluetooth");

        // Load current config (ID 1) or default to new Configuration()
        config = configurationRepository.findById(1).orElse(new Configuration());
        // .orElse = 'config = configurationRepository.findById(1).get();' but returns other value if ID doesn't exist

        System.out.println("CONFIG : " + config.getAirport() + " " + config.getLowTemp());
        if (DEMO) System.out.println("DEMO");
    }

    @Scheduled(initialDelay = 5 * 1000, fixedDelay = 15 * 60 * 1000)
    public void repeat15m() {
        logger.info("Repeat 15M");
        outside.update(config.getAirport());
    }

    @Scheduled(initialDelay = 17 * 1000, fixedDelay = 60 * 1000)
    public void repeat1m() {
        logger.info("Repeat 1M");

        float sp = 0;

        Calendar rightNow = Calendar.getInstance();
        int year=rightNow.get(Calendar.YEAR)-2000;
        int month=rightNow.get(Calendar.MONTH) + 1;
        int dayMonth=rightNow.get(Calendar.DAY_OF_MONTH);
        int hour = rightNow.get(Calendar.HOUR_OF_DAY);
        int min = rightNow.get(Calendar.MINUTE);
        int dayWeek = rightNow.get(Calendar.DAY_OF_WEEK);
        int hm = 100 * hour + min;

        float personSP = 0;
        for (User user : userRepository.findAll()) {
            boolean isHome = user.isHome();
            for (Rule rule : ruleRepository.findAll(Sort.by("hour", "min").ascending())) {
                if (rule.getUser().getId() == user.getId()) {

                    // check if value rule < lowTemp, fix db : no values lower then lowTemp
                    if (rule.getTemp()<config.getLowTemp()) {
                        rule.setTemp(config.getLowTemp());
                        ruleRepository.save(rule);
                    }

                    if (isHome) {
                        int rday = rule.getDay();
                        int rhm = 100 * rule.getHour() + rule.getMin();

                        // 0 = elke dag
                        // 1 ... 7 = zondag ... zaterdag 
                        if (dayWeek == rday || rday == 0) {
                            if (hm >= rhm) {
                                data.add("Rule : " + rhm);
                                personSP = rule.getTemp();
                            }
                        }
                    }
                }
            }

            if (sp < config.getLowTemp()) {
                sp = config.getLowTemp();
            }
            if (personSP > sp) {
                sp = personSP;
            }
        }

        int outsideTemp = outside.getTemperature();
        int outsidePressure = outside.getPressure();
        data.add("Outside Temperature = " + outsideTemp);
        data.add("Outside Atmospheric Pressure = " + outsidePressure);
        if (SERIAL.open()) {
            // set current outside temp / YY / MM / DD / hh / mm
            if (oldOA != outsidePressure) {
                if (SERIAL.setOA(outsidePressure)) oldOA = outsidePressure;
            }

            if (oldOT != outsideTemp) {
                if (SERIAL.setOT(outsideTemp)) oldOT = outsideTemp;
            }

            if (oldYear != year) {
                if (SERIAL.setYY(year)) oldYear = year;
            }

            if (oldMonth != month) {
                if (SERIAL.setMM(month)) oldMonth = month;
            }

            if (oldDay != dayMonth) {
                if (SERIAL.setDD(dayMonth)) oldDay = dayMonth;
            }

            if (oldHour != hour) {
                if (SERIAL.sethh(hour)) oldHour = hour;
            }

            SERIAL.setmm(min);

            // get status of thermostats relais (OutPut + Man/Auto)
            int op = SERIAL.getOP();
            data.add("" + op);
            int count = 20;
            while (!(op == 0 || op == 1 || op == 10 || op == 11)) {
                communicationError("RETRY");
                op = SERIAL.getOP();
                data.add("" + op);
                if (count-- < 0) break;
            }

            // get inside temperature (Process Value)
            float pv = SERIAL.getPV();
            data.add("" + pv);
            count = 20;
            while (pv == -1f) {
                communicationError("RETRY");
                pv = SERIAL.getPV();
                data.add("" + pv);
                if (count-- < 0) break;
            }

            // Lower SP if it's warmer outside then SP
            if (outsideTemp > sp) {
                sp = 2 * sp - outsideTemp;
            }
            // Lower SP if it's warmer outside then inside (PV)
            if (outsideTemp > pv) {
                sp = sp + pv - outsideTemp;
            }
            // Lower SP if it's warmer outside then config LowTemperature
            if (outsideTemp > config.getLowTemp()) {
                sp = sp + config.getLowTemp() - outsideTemp;
            }
            // Lower SP if Pressure > 1020 (high pressure = nice weather)
            if (outsidePressure > 1020 ) {
                float p = outsidePressure - 1000;
                p = p /100;
                sp = sp - p;
            }
            // No negative SP
            if (sp < 0) {
                sp = 0;
            }

            // Round SP .x
            sp = Math.round(sp * 10f) / 10f;

            // set SP
            if (op == 0 || op == 10) {
                // MAN = get SP from thermostat
                sp = SERIAL.getSP();
                data.add("" + sp);
            } else {
                // AUTO = set SP
                data.add("Control SP " + sp);
                count = 20;
                while (SERIAL.getSP() != sp) {
                    while (!SERIAL.setSP(sp)) {
                        communicationError("RETRY");
                        if (count-- < 0) break;
                    }
                    if (count-- < 0) break;
                    data.add("OK");
                    if (DEMO) break;
                }
                if (count >= 0) data.add("Control SP OK");
            }

            // update PV SP for HTML
            DataList.setPv(pv);
            DataList.setSp(sp);
            DataList.setOp(op);

        } else {
            communicationError("No BT connection");
        }
    }

    private void communicationError(String string) {
        data.add(string);
        oldOA = oldOT = oldYear = oldMonth = oldDay = oldHour = -1;
        SERIAL.close();
    }
}
