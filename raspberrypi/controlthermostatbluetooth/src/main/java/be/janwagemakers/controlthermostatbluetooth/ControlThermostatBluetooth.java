package be.janwagemakers.controlthermostatbluetooth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ControlThermostatBluetooth {

	public static void main(String[] args) {
		SpringApplication.run(ControlThermostatBluetooth.class, args);
	}
        
    @Bean
    public StartupRunner schedulerRunner() {
        return new StartupRunner();
    }
}

