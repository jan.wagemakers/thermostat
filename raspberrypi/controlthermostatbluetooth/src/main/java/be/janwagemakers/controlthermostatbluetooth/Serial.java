/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.janwagemakers.controlthermostatbluetooth;

import com.fazecast.jSerialComm.SerialPort;

import java.text.DecimalFormat;

/**
 *
 * @author jan
 */
class Serial {
    private final SerialPort rfcomm;
    private final static boolean DEMO = Config.DEMO;
    private DataList data = new DataList();
    private boolean open;

    Serial() {
        open = false;
        rfcomm = SerialPort.getCommPort("/dev/rfcomm0");
        rfcomm.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING |
                                  SerialPort.TIMEOUT_WRITE_BLOCKING, 4000, 0);
    }
    
    public boolean open() {
        if (DEMO) return true;
        if (!open) {
            data.add("---------------");
            data.add("open");
            open = rfcomm.openPort();
        }
        return open;
    }

    public boolean close() {
        if (DEMO) return true;
        if (open) {
            data.add("---------------");
            data.add("close");
            open = !rfcomm.closePort();
        }
        return !open;
    }

    private boolean tx(String command, float value, DecimalFormat dec) {
        String tx = command + dec.format(value);
        float rx=get(tx);
        if( value != rx ) {
            data.add("ERROR");
            return false;
        }
        return true;
    }
    
    boolean setSP(float temp) {
        if (temp < 0 ) return false;
        DecimalFormat dec = new DecimalFormat("00.0");
        return tx("SP=", temp, dec);
    }

    boolean setOT(int temp) {
        DecimalFormat dec = new DecimalFormat("00");
        return tx("OT=", temp, dec);

    }

    boolean setOA(int p) {
        DecimalFormat dec = new DecimalFormat("0000");
        return tx("OA=", p, dec);
    }

    boolean setYY(int number) {
        DecimalFormat dec = new DecimalFormat("00");
        return tx("YY=", number, dec);
    }

    boolean setMM(int number) {
        DecimalFormat dec = new DecimalFormat("00");
        return tx("MM=", number, dec);
    }

    boolean setDD(int number) {
        DecimalFormat dec = new DecimalFormat("00");
        return tx("DD=", number, dec);
    }

    boolean sethh(int number) {
        DecimalFormat dec = new DecimalFormat("00");
        return tx("hh=", number, dec);
    }

    void setmm(int number) {
        DecimalFormat dec = new DecimalFormat("00");
        tx("mm=", number, dec);
    }

    private float get(String tx) {
        float rx;
        data.add("---------------");
        data.add("CMD : " + tx);

        if (DEMO) {
            if (tx.length() > 3) return Float.parseFloat(tx.substring(3));
            if (tx.equals("OP?")) {
                if ((int)(Math.random()+.5) == 1) {
                    return 1.1f;
                } else {
                    return 0.1f;
                }
            }
            return (int)(Math.random() * 14 + 16);
        }

        open();
        try {
            byte[] in = new byte[10];
            // read to clear any data that's left on the line
            rfcomm.readBytes(in, in.length);
            
            // send xxx to thermostat
            byte[] out = tx.getBytes();
            rfcomm.writeBytes(out, tx.length());
            byte[] enter = {13};
            rfcomm.writeBytes(enter, 1);

            // read result from thermostat
            rfcomm.readBytes(in, in.length);
            rx = Float.parseFloat(new String(in));
        } catch (Exception e) {
            data.add(e.getMessage());
            return -1;
        }
        return rx;
    }
    
    float getSP() {
        return get("SP?");
    }
    
    float getPV() {
        return get("PV?");
    }

    int getOP() {
        return Math.round(get("OP?") * 10f);
        // 00 = Relay off - MAN
        // 10 = Relay on  - MAN
        // 01 = Relay off - AUTO
        // 11 = Relay on  - AUTO
    }
}
   