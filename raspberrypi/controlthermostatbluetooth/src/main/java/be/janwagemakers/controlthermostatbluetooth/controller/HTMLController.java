/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.janwagemakers.controlthermostatbluetooth.controller;

import be.janwagemakers.controlthermostatbluetooth.Config;
import be.janwagemakers.controlthermostatbluetooth.DataList;
import be.janwagemakers.controlthermostatbluetooth.Entity.Configuration;
import be.janwagemakers.controlthermostatbluetooth.Entity.Rule;
import be.janwagemakers.controlthermostatbluetooth.Entity.User;
import be.janwagemakers.controlthermostatbluetooth.Outside;
import be.janwagemakers.controlthermostatbluetooth.Repository.ConfigurationRepository;
import be.janwagemakers.controlthermostatbluetooth.Repository.RuleRepository;
import be.janwagemakers.controlthermostatbluetooth.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author jan
 */
@org.springframework.stereotype.Controller
public class HTMLController {

    @Autowired private ConfigurationRepository configurationRepository;
    @Autowired private UserRepository userRepository;
    @Autowired private RuleRepository ruleRepository;
    @Autowired private Outside outside;
    @Autowired private DataList data;

    private final static boolean DEMO = Config.DEMO;
    private User user = new User("New User");

    @GetMapping("/")
    public String index(Model model) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm:ss");
        Date date = new Date();
        String clock = dateFormat.format(date);
        if (DEMO) clock = clock + " DEMO";
        model.addAttribute("clock", clock);
        model.addAttribute("sp", DataList.getSp());
        model.addAttribute("pv", DataList.getPv());
        model.addAttribute("op", DataList.getOp()/10);                      // 0X / 1X
        model.addAttribute("auto", DataList.getOp()-(DataList.getOp()/10)*10);  // X0 / X1
        model.addAttribute("outsideTemp", outside.getTemperature());
        return "index";
    }

    @GetMapping("/log")
    public String log(Model model) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String clock = dateFormat.format(date);
        if ((Integer.parseInt(clock.substring(clock.length() - 1)) & 1) == 0 ) {
            clock = clock + " |";
        } else {
            clock = clock + " | _";
        }
        model.addAttribute("clock", clock);
        model.addAttribute("data", data.getData());
        return "log";
    }

    @GetMapping("/users")
    private String users(Model model) {
        model.addAttribute("data", data.getData());
        model.addAttribute("users", userRepository.findAll());
        model.addAttribute("rules", ruleRepository.findAll());
        model.addAttribute("newUser", new User("New User"));
        model.addAttribute("newRule", new Rule(new User("New User"), 4,5,6,16.0f));
        return "users";
    }

    @GetMapping("/config")
    private String config(Model model) {
        model.addAttribute("config", configurationRepository.findById(1).orElse(new Configuration()));
        return "config";
    }

    @GetMapping("/rules")
    private String rules(Model model) {
        List<Rule> rules = new ArrayList<>();
        for (Rule rule : ruleRepository.findAll(Sort.by("day", "hour", "min").ascending())) {
            if (rule.getUser().getId() == user.getId()) {
                rules.add(rule);
            }
        }
        model.addAttribute("newRule", new Rule(user,0,0,0,16.0f));
        model.addAttribute("user", user.getName());
        model.addAttribute("min", configurationRepository.findById(1).orElse(new Configuration()).getLowTemp());
        model.addAttribute("rules", rules);
        return "rules";
    }

    @GetMapping("/txt")
    public void txt(HttpServletResponse response) throws IOException {
        String mode="?";
        String relay="?";
        switch (DataList.getOp()) {
            case 0:
                mode="MAN";
                relay="OFF";
                break;
            case 1:
                mode="AUTO";
                relay="OFF";
                break;
            case 10:
                mode="MAN";
                relay="ON";
                break;
            case 11:
                mode="AUTO";
                relay="ON";
                break;
        }
        String text = "SP=" + DataList.getSp() +
                " PV=" + DataList.getPv() +
                " OT=" + outside.getTemperature() +
                " " + mode + " " + relay +
                "\n";
        OutputStream outputStream = response.getOutputStream();
        outputStream.write(text.getBytes(StandardCharsets.UTF_8));
        outputStream.close();
    }

    @PostMapping("/users")
    public String user(@ModelAttribute User newUser, @RequestParam String action, @RequestParam String id, Model model ) {
        switch (action) {
            case "Add" :
            case "Save" :
                if (!DEMO) newUser = userRepository.save(newUser);
                break;
            case "Del" :
                if (!DEMO) userRepository.deleteById(Integer.parseInt(id));
                break;
            case "Rules" :
                user = userRepository.findById(Integer.parseInt(id)).get();
                return rules(model);
        }
        return users(model);
    }

    @PostMapping("/config")
    public String config(@ModelAttribute Configuration newConfig, Model model ) {
        if (!DEMO) configurationRepository.save(newConfig);
        return config(model);
    }

    @PostMapping("/rule")
    public String rule(@ModelAttribute Rule newRule, @RequestParam String action, @RequestParam String line, Model model ) {
        switch (action) {
            case "Add" :
            case "Save" :
                if (!DEMO) newRule = ruleRepository.save(newRule);
                break;
            case "Del" :
                if (!DEMO) ruleRepository.deleteById(Integer.parseInt(line));
               break;
        }
        return rules(model);
    }
}
