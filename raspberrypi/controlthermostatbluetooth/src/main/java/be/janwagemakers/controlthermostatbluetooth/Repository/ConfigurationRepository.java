/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.janwagemakers.controlthermostatbluetooth.Repository;

import be.janwagemakers.controlthermostatbluetooth.Entity.Configuration;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author jan
 */
// @RepositoryRestResource
public interface ConfigurationRepository extends PagingAndSortingRepository<Configuration, Integer>{

}

