package be.janwagemakers.controlthermostatbluetooth;

import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

@Component("DataList")
public class DataList {
    private static ArrayList<String> data = new ArrayList<>();
    private static float pv = 0;
    private static float sp = 0;
    private static int   op = 0;

    public ArrayList<String> getData() {
        return data;
    }

    public void add(String string) {
        System.out.println(string);

        Calendar rightNow = Calendar.getInstance();
        int hr = rightNow.get(Calendar.HOUR_OF_DAY);
        int mn = rightNow.get(Calendar.MINUTE);
        int sc = rightNow.get(Calendar.SECOND);
        int d = rightNow.get(Calendar.DAY_OF_MONTH);
        int m = 1 + rightNow.get(Calendar.MONTH);
        int y = rightNow.get(Calendar.YEAR);

        DecimalFormat dec = new DecimalFormat("00");
        data.add(y + "/" + dec.format(m) + "/" + dec.format(d) + " " + dec.format(hr) + ":" + dec.format(mn) + ":" + dec.format(sc) + " | " + string);
        if (data.size() > 150) {
            data.remove(0);
        }
    }

    public static float getSp() {
        return sp;
    }

    public static float getPv() {
        return pv;
    }

    public static int getOp() {
        return op;
    }

    static void setPv(float pv) {
        DataList.pv = pv;
    }

    static void setSp(float sp) {
        DataList.sp = sp;
    }

    public static void setOp(int op) {
        DataList.op = op;
    }
}
