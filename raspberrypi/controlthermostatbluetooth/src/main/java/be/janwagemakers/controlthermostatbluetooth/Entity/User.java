/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.janwagemakers.controlthermostatbluetooth.Entity;

import be.janwagemakers.controlthermostatbluetooth.DataList;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author jan
 */
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    @OneToMany(mappedBy = "user")
    private List<Rule> rules;
    private String bluetooth;
    private String ip;

    protected User() {}

    public User(String name) {
        this.name = name;
        this.bluetooth = "";
        this.ip = "";
    }
    
    public boolean isHome() {
        DataList data = new DataList();
        data.add("---------------");
        data.add(getName() + " | GSM: IP[" + getIp() + "] BT[" + getBluetooth() + "]");

        // No check for GSM --> return home = true
        if (getBluetooth().equals("") && getIp().equals("")) {
            data.add("No GSM configured --> " + getName() + " is home.");
            return true;
        }

        String s;
        Boolean home = false;

        // Check GSM with IP
        if (!getIp().equals("")) {
            try {
                Process p = Runtime.getRuntime().exec("ping -c4 " + getIp());
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                //BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

                // read the output from the command
                while (p.isAlive()) {
                    while ((s = stdInput.readLine()) != null) {
                        data.add("-- " + s);
                    }
                }

                // get exitValue : 0 --> ping was OK
                if (p.exitValue() == 0) {
                    home = true;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Check GSM with bluetooth.
        if (!home && !getBluetooth().equals("")) {
            try {
                Process p = Runtime.getRuntime().exec("hcitool name " + getBluetooth());
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                //BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

                // read the output from the command
                while ((s = stdInput.readLine()) != null) {
                    data.add("-- " + s);
                    home = true;
                }

                // read any errors from the attempted command
                // while ((s = stdError.readLine()) != null) { }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (home) {
            data.add("GSM detected --> " + getName() + " is home.");
        } else {
            data.add("GSM not detected --> " + getName() + " is away.");
        }
        return home;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBluetooth() {
        return bluetooth;
    }

    public void setBluetooth(String bluetooth) {
        this.bluetooth = bluetooth;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
    

