document.addEventListener("DOMContentLoaded", function(e) {
    var t = document.getElementById("pull"),
        n = document.querySelector("nav ul");
    t.addEventListener("click", function(e) {
        n.classList.toggle("hide")
    }), window.addEventListener("scroll", function() {
        var e = window.pageYOffset | document.body.scrollTop;
        document.getElementById("main").style.backgroundPosition = "100% " + parseInt(-e / 3) + "px, 0%, center top"
    })
});