; vim: set filetype=pic:
; ---------------------------------------------------------------------------
; µC = PIC18F1320 
; ---------------------------------------------------------------------------
        LIST P=18F1320
        #include <p18f1320.inc> 
	ERRORLEVEL 1
; ---------------------------------------------------------------------------
; Configuration
; ---------------------------------------------------------------------------
        CONFIG OSC      = INTIO2        ; Internal RC, OSC1 as RA7, OSC2 as RA6
        CONFIG WDT      = ON            ; Watchdog Timer
	CONFIG WDTPS    = 32768		; Watchdog Timer Postscale Select bits
	CONFIG BOR      = OFF           ; Brown-Out Reset
        CONFIG MCLRE    = OFF           ; MCLR Enable
        CONFIG LVP      = OFF           ; Low Voltage ICSP
        CONFIG DEBUG    = OFF           ; Background Debugger Enable

; ---------------------------------------------------------------------------
; Constants
; ---------------------------------------------------------------------------
	#DEFINE	OFFSET	D'24'		; Temperature correction,
					; The DS1820 is accurate, but
					; when build into a case,
					; the temperature measured can be a
					; litle higher then outside the
					; case.
					;
					; 24/16 = 1.5°C

; ---------------------------------------------------------------------------
; Define LCD pins
; ---------------------------------------------------------------------------
        #DEFINE RS      PORTA, 4        ; RS line of LCD
        #DEFINE E       PORTA, 6        ; E line of LCD
        #DEFINE D4      PORTA, 0        ;
        #DEFINE D5      PORTA, 1        ; D4-D7 of LCD
        #DEFINE D6      PORTA, 2        ;
        #DEFINE D7      PORTA, 3        ;

; ---------------------------------------------------------------------------
; Define thermostat related pins
; ---------------------------------------------------------------------------
        #DEFINE RELAY   PORTA, 7        ; Output heating ON/OFF
	#DEFINE	MAN	PORTA, 5	; 0 = AUTO / 1 = MAN
; ---------------------------------------------------------------------------
; DEFINE DS18B20 pin
; ---------------------------------------------------------------------------
        #DEFINE DQ      5               ; PORTB RB5

; ---------------------------------------------------------------------------
; MACRO's DS18B20
; ---------------
;       See <http://www.maximintegrated.com/en/app-notes/index.mvp/id/2420>
; ---------------------------------------------------------------------------
OW_HIZ: MACRO   ; Force the DQ line into a high impedance state.
                BSF    TRISB, DQ        ; Make DQ pin High Z
        ENDM

OW_LO:  MACRO   ; Force the DQ line to a logic low.
                BCF    PORTB, DQ        ; Clear the DQ bit
                BCF    TRISB, DQ        ; Make DQ pin an output
        ENDM

WAIT:   MACRO   TIME
                ; Delay for TIME µs.
                ; Variable time must be in multiples of 5µs.
                MOVLW  (TIME/5) - 1      ; 1µs to process
                MOVWF  TMP0              ; 1µs to process
                CALL   WAIT5U            ; 2µs to process
        ENDM

; ---------------------------------------------------------------------------
; MACRO's LCD
; ---------------------------------------------------------------------------
EStrobe MACRO                           ; Strobe the "E" Bit
                bsf    E
                bcf    E
        ENDM

PRINT   MACRO   var                     ; load TBLPTR with string address
                movlw  upper(var)       ; for LCD display
                movwf  TBLPTRU
                movlw  high(var)
                movwf  TBLPTRH
                movlw  low(var)
                movwf  TBLPTRL
                tblrd*+
                call   printstr
        ENDM

; ---------------------------------------------------------------------------
; MACRO wait ms
; ---------------------------------------------------------------------------
WAITms  MACRO   TIME
                ; Delay for TIME ms
                movlw  TIME
                call   Wachtms
        ENDM

; ---------------------------------------------------------------------------
; Variables
; ---------------------------------------------------------------------------
        cblock  0x00
		heart
		delay
                Temp
                dummy
             	; conv numbers --> char
                hundreds
                bin
                tens_and_ones
 		; temp.display
                MEM
                ; fraction
                ; DS18B20
                TMP0
                PDBYTE
                IOBYTE
                COUNT
                ; Thermostat 
                H_TEMP                  ; Used to convert and print temperature
                L_TEMP
                ;
                H_SOLL                  ; Temp gewenste waarde
                L_SOLL
                ;
                H_IST                   ; Temp huidige waarde
                L_IST
		;
		ASCII_TEMP10		; Converted Temp Ten's
		ASCII_TEMP1		; Converted Temp One's
		ASCII_FRACTION		; Converted Temp Fraction
		;
                FILTER
                FILTER_COUNTER
		;
		thermowait
		thermowait2
		;
		btCounter
		;
		YY			; Year - 2000
		YY1	
		;
		MM			; Month
		MM1
		;
		DD			; day
		DD1
		;
		hh			; hours
		hh1
		;
		mm			; minutes
		mm1
		;
		P1			; pressure
		P2
		P3
		P4
		;
		btIN			; 0 : S	S P O O Y M D h m
		btIN1			; 1 : P P V P T Y M D h m
		btIN2			; 2 : = ? ? ? = = = = = =
		btIN3			; 3 : 2	      - 1 0 1 2 4
		btIN4			; 4 : 0       1 9 2 1 1 7
		btIN5			; 5 : .       0
		btIN6			; 6 : 7       .
        	btIN7			;	      0		
		btIN8			;	    
		btIN9			;
	endc

; ---------------------------------------------------------------------------
; START
; ---------------------------------------------------------------------------
start
	movlw   B'01100000'             ; FREQ = 4Mhz
	movwf   OSCCON
	
	CLRF    PORTA                   ; Initialize PORTA by clearing output
        CLRF    PORTB                   ; Initialize PORTB by clearing output
                                        ; data latches
	movlw	B'00010001'		; VREF = VDD/VSS ; Channel 4 selected
	movwf	ADCON0	
	movlw	B'01101111'		; RB0/AN4 = analog input
	movwf	ADCON1
	movlw	B'10111110'
	movwf	ADCON2			; right justified / TADJ / FOSC 

	movlw	B'00100000'
	movwf	TRISA			; 0 = OUTPUT	; 1 = INPUT
	movlw	B'00010011'
	movwf	TRISB

	WAITms	10			; Wait a few ms

 	movlw   D'1'                    ; SETPOINT = 20.69
        movwf   H_SOLL
        movlw   D'75'
        movwf   L_SOLL

        movlw   D'90'                   ; FILTER
        movwf   FILTER
        movlw   D'1'
        movwf   FILTER_COUNTER
	movwf	thermowait
	movwf	thermowait2

	movlw	"?"			; init date/time display
	movwf	YY
	movwf	YY1
	movwf	MM
	movwf	MM1
	movwf	DD
	movwf	DD1
	movwf	hh
	movwf	hh1
	movwf	mm
	movwf	mm1

	call	Wacht1s

        call    initlcd                 ; setup LCD display
	
	movlw	H'40'			; create custom chars
	call	SendINS	

        movlw	D'4'			; 0 : thermometer symbol
	call	SendCHAR
	movlw	D'10'
	call	SendCHAR
	movlw	D'10'
	call    SendCHAR
	movlw	D'10'
	call    SendCHAR
	movlw	D'17'
	call    SendCHAR
	movlw	D'31'
	call	SendCHAR
	movlw	D'14'
	call	SendCHAR
	movlw	D'0'
	call 	SendCHAR

        movlw	D'24'			; 1 : °C
	call	SendCHAR
	movlw	D'24'
	call	SendCHAR
	movlw	D'3'
	call    SendCHAR
	movlw	D'4'
	call    SendCHAR
	movlw	D'4'
	call    SendCHAR
	movlw	D'4'
	call	SendCHAR
	movlw	D'3'
	call	SendCHAR
	movlw	D'0'
	call 	SendCHAR

        movlw	D'8'			; 2 : SP
	call	SendCHAR
	movlw	D'16'
	call	SendCHAR
	movlw	D'8'
	call    SendCHAR
	movlw	D'19'
	call    SendCHAR
	movlw	D'3'
	call    SendCHAR
	movlw	D'2'
	call	SendCHAR
	movlw	D'2'
	call	SendCHAR
	movlw	D'0'
	call 	SendCHAR

        movlw	D'0'			; 3 : open hart
	call	SendCHAR
	movlw	D'0'
	call	SendCHAR
	movlw	D'10'
	call    SendCHAR
	movlw	D'21'
	call    SendCHAR
	movlw	D'17'
	call    SendCHAR
	movlw	D'10'
	call	SendCHAR
	movlw	D'4'
	call	SendCHAR
	movlw	D'0'
	call 	SendCHAR

        movlw	D'0'			; 4 : full hart
	call	SendCHAR
	movlw	D'0'
	call	SendCHAR
	movlw	D'10'
	call    SendCHAR
	movlw	D'31'
	call    SendCHAR
	movlw	D'31'
	call    SendCHAR
	movlw	D'14'
	call	SendCHAR
	movlw	D'4'
	call	SendCHAR
	movlw	D'0'
	call 	SendCHAR

	movlw	D'31'			; 5 : flag
	call    SendCHAR
 	movlw   D'31'
	call	SendCHAR
	movlw   D'31'
	call	SendCHAR
	movlw   D'31'
	call	SendCHAR
	movlw   D'16'
	call	SendCHAR
	movlw   D'16'
	call	SendCHAR
	movlw   D'16'
	call	SendCHAR
	movlw   D'16'
	call	SendCHAR

	movlw   H'80'                   ; goto line 1
        call    SendINS
        PRINT   jan                     ; PRINT jan

	; From Page 145 of the PIC manual
	; 
	; 16.3.2 EUSART ASYNCHRONOUS RECEIVER

	; The receiver block diagram is shown in Figure 16-5.
	; The data is received on the RB4/AN6/RX/DT/KBI0 pin
	; and drives the data recovery block. The data recovery
	; block is actually a high-speed shifter, operating at x16
	; times the baud rate, whereas the main receive serial
	; shifter operates at the bit rate or at FOSC. This mode
	; would typically be used in RS-232 systems.
	; To set up an Asynchronous Reception:

	; 1. Initialize the SPBRGH:SPBRG registers for the
	; appropriate baud rate. Set or clear the BRGH
	; and BRG16 bits, as required, to achieve the
	; desired baud rate.

	; movlw	D'51'		; 1200 BAUD
	; movlw	D'207'		; 300 BAUD
	movlw 	D'25'		; 2400 BAUD
	movwf	SPBRG
	clrf	SPBRGH
	bcf 	BAUDCTL, BRG16
	bcf 	TXSTA, BRGH

	; 2. Enable the asynchronous serial port by clearing
	; bit SYNC and setting bit SPEN.

	bcf	TXSTA, SYNC
	bsf     RCSTA, SPEN             
	
	; 5. R: Enable the reception by setting bit CREN.
	bsf     RCSTA, CREN

	; 5. T: Enable  the  transmission  by  setting  bit  TXEN, which will also set bit TXIF.
	bsf	TXSTA, TXEN 

again:
	clrf	btCounter	; btCounter = 0
	; characters --> buffer
	lfsr	0,  btIN
	
read:
	call	thermostat

	; 6. Flag bit, RCIF, will be set when reception is
	; complete and an interrupt will be generated if
	; enable bit RCIE was set.
	btfss	PIR1, RCIF
	bra	read

	; 8. Read the 8-bit received data by reading the
	; RCREG register.

	movff   RCREG, INDF0 	; --> buffer
	call	clearUART

	movlw	D'9'		; W = 9
	cpfslt	btCounter
	bra	again		; if W => 9 then avoid buffer overflow
				; (ignore received chars) 
	incf	btCounter	; btCounter++
	
	movlw   D'13'
	cpfseq  POSTINC0 	; ENTER?
	bra     read     
	; ENTER is gegeven

	movlw   H'D4'
        call    SendINS         ; goto line 4 off LCD	
	PRINT	spc20		; clear line 4

	call	printManAuto

	; show the given command on the display
	lfsr	0, btIN
dsploop:
	movlw	D'13'
	cpfseq	INDF0
	bra	disp
	bra	dispDone
disp:
	movf	POSTINC0, w	
	call	SendCHAR
	bra 	dsploop
dispDone:
	movlw	" "
	call	SendCHAR

	movlw	"?"
	cpfseq  btIN2 
	bra 	notQuestion  
; -------------------------------------------------------------------------	
; Command with ? | XX?
; -------------------------------------------------------------------------	
question:
	movlw 	"P"
	cpfseq  btIN
	bra 	question2
	movlw   "V"
	cpfseq  btIN1
	bra 	question2
	; -----------------------------------------------------------------
	; Command PV?
	; -----------------------------------------------------------------
	; send IST via serial line
	movff   H_IST, H_TEMP
        movff   L_IST, L_TEMP
sendCommon:	
	call	convTemp
	
	movf	ASCII_TEMP10, w
	call	tx	

	WAITms	D'250'

	movf	ASCII_TEMP1, w
	call	tx

	WAITms	D'250'

	movlw   "."
	call	tx

	WAITms  D'250'

	movf	ASCII_FRACTION, w
        call    tx

	bra	sendRETURN        
question2:
	movlw   "S"
	cpfseq  btIN
        bra     question3
	movlw   "P"
	cpfseq  btIN1 
	bra     question3
	; -----------------------------------------------------------------
	; Command SP?
	; -----------------------------------------------------------------
sendSP:
	; send SOLL (SP) via serial line
        movff   H_SOLL, H_TEMP
        movff   L_SOLL, L_TEMP
	bra 	sendCommon 
question3:
	movlw   "O"
        cpfseq  btIN
        bra     question4
        movlw   "P"
        cpfseq  btIN1
        bra     question4
	; -----------------------------------------------------------------
        ; Command OP?
        ; -----------------------------------------------------------------
	movlw	"0"
	btfsc	RELAY
	movlw	"1"	
	call	tx		; OFF/ON = 0.X/1.X

	WAITms  D'250'

	movlw	"."
	call	tx

	WAITms  D'250'

	movlw	"0"
	btfss	MAN
	movlw	"1"
	call	tx		; MAN/AUTO = X.0/X.1
		
	bra	sendRETURN

question4:
	; -----------------------------------------------------------------
	; add check's for other commands below
	; -----------------------------------------------------------------

	bra     sendError
; ------------------------------------------------------------------------- 
notQuestion:
	movlw 	"="
	cpfseq  btIN2
	bra     sendError
; -------------------------------------------------------------------------	
; Command with = | XX=XXXX
; -------------------------------------------------------------------------	
equal:
	movlw   "S"
        cpfseq  btIN
        bra     equal2
        movlw   "P"
        cpfseq  btIN1
        bra     equal2
	movlw	"."
	cpfseq	btIN5
	bra	equal2
	; -----------------------------------------------------------------
	; Command SP=
	; -----------------------------------------------------------------
        btfsc   MAN
        bra     skipSP
	
	movlw	"0"
	subwf	btIN3, w	; W = ASCII-number converted to binary 
	mullw 	D'10'		; PRODL = W * 10
	
	movlw   "0"
	subwf	btIN4, w	; W = ASCII-number converted to binary 
	addwf	PRODL, w	; W = W + PRODL | (W = 10 * btIN3 + btIN4)

	mullw	D'16'		; PRODH:PRODL  8bit -> 12bit XXXXXXXX.XXXX
	movff	PRODH, H_SOLL
	movff	PRODL, L_SOLL	; H_SOLL:L_SOLL = PRODH:PRODL

	movlw	"0"
	subwf	btIN6, w	; W = ASCII-number converted to binary

	mullw	D'16'		; PRODL	= w * 16 | .X = .X * 16
	movf	PRODL, w	; w = .X * 16

	; www.microchip.com/forums/m466146-p2.aspx
	mullw	D'205'		; W = W * 205/2048 | W = W / 10
	movf 	PRODH, w
	mullw	D'32'		; shift W 3 bits to the right
	movf	PRODH, w

	; w = .X * 16 / 10 | digit converted to fraction

	iorwf	L_SOLL		; add fraction to L_SOLL
skipSP:
	bra	sendSP
IsItOA:
	movlw	"A"
	cpfseq	btIN1
	bra	equal3
	; -----------------------------------------------------------------
        ; Command OA=		; yes, I do write spaghetti code ;-)
        ; -----------------------------------------------------------------
	movf    btIN3, w
        movwf   P1
        call    tx
        WAITms  D'250'
        movf    btIN4, w
        movwf   P2
        call    tx
        WAITms  D'250'
	movf    btIN5, w
        movwf   P3
        call    tx
        WAITms  D'250'
        movf    btIN6, w
        movwf   P4
        call    tx
        WAITms  D'250'
	call	updateHPA	
	bra	sendRETURN
equal2:
	movlw   "O"
        cpfseq  btIN
        bra     equal3
        movlw   "T"
        cpfseq  btIN1
        bra     IsItOA
	; -----------------------------------------------------------------
	; Command OT=
	; -----------------------------------------------------------------
	clrf	btIN		; use btIN as flag
OTLOOP:
	lfsr 	0, btIN3
OT:
	movlw	D'13'
	cpfseq	INDF0		; ENTER?
	bra	OTNEXT
	btfsc	btIN, 0		; First or second time we enter the loop?
	bra	OTEND		; second --> end loop
	incf	btIN		; btIN = 1
	
	movlw   H'94'           ; goto line 3 of LCD
        call    SendINS
	PRINT	spc20		; clear line 3
	call	updateHPA
	bra	OTLOOP
OTEND:	
	call	printCelsius
	bra	sendRETURN
OTNEXT:
	WAITms	D'250'
	movf	POSTINC0, w
	btfss	btIN, 0		; only send back to BT the first time
	movwf	TXREG		; Send OT back to BlueTooth
	call	SendCHAR	; Send OT to display
	bra     OT     
equal3:
	movlw   "Y"
        cpfseq  btIN
        bra     equal4
        cpfseq  btIN1
        bra     equal4
	; -----------------------------------------------------------------
        ; Command YY=
        ; -----------------------------------------------------------------
	lfsr	0, YY
	call	updateDateTime
	bra	sendRETURN
equal4:
        movlw   "M"
        cpfseq  btIN
        bra     equal5
        cpfseq  btIN1
        bra     equal5
        ; -----------------------------------------------------------------
        ; Command MM=
        ; -----------------------------------------------------------------
        lfsr	0, MM
        call	updateDateTime
	bra     sendRETURN
equal5:
        movlw   "D"
        cpfseq  btIN
        bra     equal6
        cpfseq  btIN1
        bra     equal6
        ; -----------------------------------------------------------------
        ; Command DD=
        ; -----------------------------------------------------------------
        lfsr	0, DD
	call	updateDateTime
        bra     sendRETURN
equal6:
        movlw   "h"
        cpfseq  btIN
        bra     equal7
        cpfseq  btIN1
        bra     equal7
        ; -----------------------------------------------------------------
        ; Command hh=
        ; -----------------------------------------------------------------
        lfsr	0, hh
        call	updateDateTime
	bra     sendRETURN
equal7:
        movlw   "m"
        cpfseq  btIN
        bra     equal8
        cpfseq  btIN1
        bra     equal8
        ; -----------------------------------------------------------------
        ; Command mm=
        ; -----------------------------------------------------------------
        lfsr	0, mm
        call	updateDateTime

	; -----------------------------------------------------------------
	; refresh date-time display (only after mm= command)
	; -----------------------------------------------------------------
	movlw   H'80'                   ; goto line 1
        call    SendINS
	movlw	" "
	call	SendCHAR
	movlw	" "
	call	SendCHAR
        movlw   "2"
        call    SendCHAR
        movlw   "0"
        call    SendCHAR

	movf	YY, w
	call	SendCHAR
	movf	YY1, w
	call	SendCHAR

        movlw   "/"
        call    SendCHAR

        movf    MM, w
        call    SendCHAR
        movf    MM1, w
        call    SendCHAR

        movlw   "/"
        call    SendCHAR

        movf    DD, w
        call    SendCHAR
        movf    DD1, w
        call    SendCHAR

        movlw   " "
        call    SendCHAR

        movf    hh, w
        call    SendCHAR
        movf    hh1, w
        call    SendCHAR

        movlw   ":"
        call    SendCHAR

        movf    mm, w
        call    SendCHAR
        movf    mm1, w
        call    SendCHAR

	movlw	" "
	call	SendCHAR
	movlw	" "
	call	SendCHAR
	bra	sendRETURN
equal8:
	; -----------------------------------------------------------------
	; add check's for other commands below
	; -----------------------------------------------------------------
 
sendError:
        movlw   "?"
	call	tx
sendRETURN:
        WAITms  D'250'
        movlw   D'13'
        movwf   TXREG
        WAITms  D'250'
        goto    again

tx:	; send X to serial port and display
	movwf	TXREG
	call	SendCHAR
	return

updateHPA:
	movlw	H'94'		; goto line 3 of LCD
	call	SendINS	
	movlw	D'5'		; flag symbol
	call 	SendCHAR
	movf	P1, w
	call	SendCHAR
 	movf    P2, w
        call    SendCHAR
 	movf    P3, w
        call    SendCHAR
 	movf    P4, w
        call    SendCHAR
	PRINT	hPa
	return

updateDateTime:
	movf	btIN3, w
	movwf	POSTINC0
	call	tx
	WAITms	D'250'
	movf    btIN4, w
        movwf   INDF0
        call	tx
        WAITms  D'250'
	return

clearUART:
        ; 9. If any error occurred, clear the error by clearing
        ; enable bit CREN.
        btfsc   RCSTA, OERR     ; overrun error?
        bcf     RCSTA, CREN     ; Yes = reset error
        bsf     RCSTA, CREN     ;       see page 135
	return

; --------------------------------------------------------------------------
; The real thermostat routine, which controls the heating relais!
; WATCHDOG timer and CLRWDT are used to reset the thermostat when the 
; devices fails to run this routine in time.
; --------------------------------------------------------------------------
thermostat:
	btfsc	ADCON0, 1		; AD conversion done?
	bra 	ADend
ADstart:
	bcf	STATUS, C
	rrcf	ADRESH, f
	rrcf	ADRESL, f		; AD conv = AD conv / 2
	
	btfss	MAN
	bra	skipMAN
	movff	ADRESH, H_SOLL		; AD --> H_SOLL/L_SOLL
	movff	ADRESL, L_SOLL		
skipMAN:
	bsf	ADCON0, 1		; start ADC
ADend:
	decfsz	thermowait, F
	return
	decfsz	thermowait2, F
	return

        movlw   D'211'
        call    SendINS                 ; goto last char of line 2
	incf	heart
	movf	heart, w
	andlw	D'1'
	bnz	nz
	movlw   D'3'
        bra     cmn
nz
        movlw   D'4'
cmn
        call    SendCHAR		; show heart beat
	
	CLRWDT				; clear the watchdog timer

        movlw   H'C0'                   ; goto line 2
        call    SendINS

        ; - print huidige temp -------------------------        
	clrw
        call    SendCHAR	; thermometer symbol
        movff   H_IST, H_TEMP
        movff   L_IST, L_TEMP
        call    printTemp
        ; ----------------------------------------------

        ; - print gewenste temp ------------------------
	movlw	D'2'
        call    SendCHAR	; SP symbol
        movff   H_SOLL, H_TEMP
        movff   L_SOLL, L_TEMP
        call    printTemp
        ; ----------------------------------------------

        ; - print status relay -------------------------
        btfsc   RELAY
        bra     ON
        PRINT   uit
        bra     printRend
ON
        PRINT   aan
        ; ----------------------------------------------

printRend:
        movf    H_SOLL, w
        cpfsgt  H_IST                 	; compare skip if HIST > HSOLL
        bra     x1
        bra     thOFF
x1
        cpfslt  H_IST                   ; compare skip if HIST < HSOLL
        bra     x2
        bra     thON
x2
        ; Hier kommen we als HSOLL = HIST
        ; Dus nu kunnen we op basis van L vergelijken
        movf    L_SOLL, w
        cpfsgt  L_IST                   ; compare skip if LIST > LSOLL
        bra     thON
        bra     thOFF
thON
        incf    FILTER_COUNTER, f
        movf    FILTER, w
        cpfsgt  FILTER_COUNTER          ; compare skip if FILTER_COUNTER > FILTER
        bra     filterON
        movff   FILTER, FILTER_COUNTER
        bsf     RELAY
	bra	spc
thOFF
        bsf     STATUS, C
        decf    FILTER_COUNTER, f
        btfsc   STATUS, C
        bra     filterOFF
        clrf    FILTER_COUNTER
        bcf     RELAY
spc
	movlw	" "
	call	SendCHAR 
        bra     thdone
filterON
        movlw	"+"
	call	SendCHAR 
        bra     thdone
filterOFF
	movlw   "-"
        call    SendCHAR
thdone
	call	printManAuto
        ;------------------------------------------------------------------------
RETRY
        CALL    OW_RESET
        BTFSS   PDBYTE,0
        BRA     RETRY                   ; Retry when there is no pulse of DS18B20
        MOVLW   H'CC'                   ; DS18B20 : Skip ROM
        CALL    DSTXBYTE
        MOVLW   H'44'                   ; DS18B20 : Convert Temp
        CALL    DSTXBYTE

        WAIT    D'800'                  ; Wait 800 µs 

        CALL    OW_RESET
        BTFSS   PDBYTE,0
        BRA     RETRY                   ; Retry when there is no pulse of DS18B20
        MOVLW   H'CC'                   ; DS18B20 : Skip ROM
        CALL    DSTXBYTE
        MOVLW   H'BE'                   ; DS18B20 : Read ScratchPad
        CALL    DSTXBYTE

        CALL    DSRXBYTE                ; Read LTEMP (in IOBYTE)
        movff   IOBYTE, MEM             ; MEM = LTEMP
        CALL    DSRXBYTE                ; READ HTEMP (in IOBYTE)

        ;==============================================================;
        ; IOBYTE = HTEMP                                               ;
        ; MEM    = LTEMP                                               ;
        ; BTW, we assume that we never meassure negative temperatures  ;
        ;==============================================================;
	
	movff   IOBYTE, H_IST
        movff   MEM, L_IST
	
  	movlw  OFFSET        		; Load temp correction constant
  	subwf  L_IST			; subtract correction
  	btfss  STATUS, C              	; If the Carry Flag is Set
   	decf   H_IST                  	; dec H__IST
	return
	
; ---------------------------------------------------------------------------
; print man auto
; ---------------------------------------------------------------------------
printManAuto:
	movlw	H'D4'
	call	SendINS			; goto line 4

	btfsc	MAN
	bra	modeMAN
	PRINT	auto
	bra	printEnd
modeMAN:
	PRINT 	man
printEnd:
	return

; ---------------------------------------------------------------------------
; print temperature in HTEMP:LTEMP
; ---------------------------------------------------------------------------
printTemp
	call	convTemp
	movf	ASCII_TEMP10, w
	call	SendCHAR
	movf	ASCII_TEMP1, w
	call	SendCHAR
	movlw   "."
        call    SendCHAR                ; PRINT .
	movf	ASCII_FRACTION, w
	call	SendCHAR
printCelsius	
	movlw	D'1'			; PRINT °C
	call	SendCHAR
	movlw	" " 			; print " "
	call	SendCHAR
	return

; ---------------------------------------------------------------------------
; convert temperature in 
; 	HTEMP:LTEMP 
; to 
; 	ASCII_TEMP10, ASCII_TEMP1, ASCII_FRACTION
; ---------------------------------------------------------------------------
convTemp
	movf	L_TEMP, W
	mullw	D'16'			; PRODH = L_TEMP/16 | PRODL = fraction * 16
	swapf	H_TEMP, w		; W = H_TEMP * 16 (ignore negative and big values)
	iorwf	PRODH, w		; W = H_TEMP:L_TEMP/16	

        call    ConvDec                 ; Convert XX

	swapf	PRODL, w		; w = fraction (PRODL/16)
	mullw	D'10'			; PRODL = fraction * 10
	movlw	D'8'			; W = 8 | 8/16 = .5
	addwf	PRODL
	movf  	PRODL, w		; W = fraction * 10 + 8
	mullw	D'16'			; PRODH = (fraction * 10 + 8)/16
	movf	PRODH, w		; w = (fraction * 10 + 8)/16 

	addlw 	'0'
	movwf	ASCII_FRACTION
        return

; ---------------------------------------------------------------------------
; LCD subroutine init/cls
; ---------------------------------------------------------------------------
initlcd:
        WAITms  D'40'
        bcf     RS                      ; send an 8 bit instruction
        movlw   0x03                    ; Reset Command
        call    NybbleOut               ; Send the Nybble
        WAITms  D'5'                    ; Wait 5 msecs before Sending Again
        EStrobe
        WAIT    D'160'                  ; Wait 160 usecs before Sending 2nd Time
        EStrobe
        WAIT    D'160'                  ; Wait 160 usecs before Sending 3rd Time
        bcf     RS                      ; send an 8 bit instruction
        movlw   0x02                    ; Set 4 Bit Mode
        call    NybbleOut
        WAIT    D'160'
        movlw   0x028                   ; 4 bit, 2 Line, 5x7 font
        call    SendINS
        movlw   0x010                   ; display shift off
        call    SendINS
        movlw   0x006                   ; increment cursor
        call    SendINS
        movlw   0x00C                   ; display on cursor off
        call    SendINS
cls:
        movlw   0x001                   ; Clear the Display RAM
        call    SendINS
        WAITms  D'5'                    ; Note, Can take up to 4.1 msecs
        return

; ---------------------------------------------------------------------------
; LCD subroutine print string
; ---------------------------------------------------------------------------
printstr:
        movf    TABLAT, w               ; get characters 
        btfsc   STATUS, Z               ; if character = 0 then ...
        return                          ;                       ... exit
        call    SendCHAR                ; display character 
        tblrd*+                         ; TABLAT = next character
        bra     printstr                ; repeat


; ---------------------------------------------------------------------------
;              Send the character in W out to the LCD                  
; ---------------------------------------------------------------------------
SendASCII
         addlw '0'                      ; Send nbr as ASCII character
SendCHAR                                ; Send the Character to the LCD
         movwf  Temp                    ; Save the Temporary Value
         swapf  Temp, w                 ; Send the High Nybble
         bsf    RS                      ; RS = 1
         call   NybbleOut
         movf   Temp, w                 ; Send the Low Nybble
         bsf    RS
         call   NybbleOut
         return

;----------------------------------------------------------------------------
;              Send an instruction in W out to the LCD                 
;----------------------------------------------------------------------------
SendINS                                 ; Send the Instruction to the LCD
        movwf  Temp                     ; Save the Temporary Value
        swapf  Temp, w                  ; Send the High Nybble
        bcf    RS                       ; RS = 0
        call   NybbleOut
        movf   Temp, w                  ; Send the Low Nybble
        bcf    RS
        call   NybbleOut
        return

;----------------------------------------------------------------------------
;              Send the nibble in W out to the LCD                     
;----------------------------------------------------------------------------
NybbleOut                               ; Send a Nybble to the LCD
        movwf   dummy                   ; dummy = W
        bcf     D7                      ; D7....D4 = dummy[3:0] 
        bcf     D6
        bcf     D5
        bcf     D4
        btfsc   dummy, 3
        bsf     D7
        btfsc   dummy, 2
        bsf     D6
        btfsc   dummy, 1
        bsf     D5
        btfsc   dummy, 0
        bsf     D4
        EStrobe                         ; Strobe out the LCD Data
        WAIT    D'160'                  ; delay for 160 usec
        return
; ---------------------------------------------------------------------------
;                  Change binary nbr in bin to BCD                     
; ---------------------------------------------------------------------------
binary_to_bcd:                          ; by Scott Dattalo
         clrf   hundreds
         swapf  bin, W
         addwf  bin, W
         andlw  B'00001111'
         skpndc
         addlw  0x16
         skpndc
         addlw  0x06
         addlw  0x06
         skpdc
         addlw  -0x06
         btfsc  bin,4
         addlw  0x16 - 1 + 0x6
         skpdc
         addlw  -0x06
         btfsc  bin,5
         addlw  0x30
         btfsc  bin, 6
         addlw  0x60
         btfsc  bin,7
         addlw  0x20
         addlw  0x60
         rlcf   hundreds, f
         btfss  hundreds, 0
         addlw  -0x60
         movwf  tens_and_ones
         btfsc  bin,7
         incf   hundreds, f
         return

; ---------------------------------------------------------------------------
;                Convert binary value in W to decimal  
; ---------------------------------------------------------------------------
ConvDec:
         movwf  bin
         call   binary_to_bcd
         ; movf hundreds, W             ; Hondertallen zijn niet nodig 
         ; call SendASCII
         swapf  tens_and_ones, W
         andlw  H'F'
	 ;
	 addlw '0'
	 movwf	ASCII_TEMP10
	 ;
	 ;;;;; call   SendASCII
         movf   tens_and_ones, W
         andlw  H'F'
	 ;
	 addlw  '0'
	 movwf  ASCII_TEMP1
	 ;
	 ;;;;; call   SendASCII
         return

;----------------------------------------------------------------------------
; Delay routines                                                       
;----------------------------------------------------------------------------
Wacht1s:
        WAITms  D'250'
        WAITms  D'250'
        WAITms  D'250'
        WAITms  D'250'
        return

Wachtms:
        movwf   delay
w2:
        WAIT    D'1000'                 ; WAIT 1000µs = 1ms
        decfsz  delay, F
        goto    w2
        return

; ---------------------------------------------------------------------------
; Subroutines DS18B20
;       see <http://www.maximintegrated.com/en/app-notes/index.mvp/id/2420>
; ---------------------------------------------------------------------------
WAIT5U:
        ;This takes 5µs to complete
        NOP                             ; 1µs to process
        NOP                             ; 1µs to process
        DECFSZ  TMP0,F                  ; 1µs if not zero or 2µs if zero
        GOTO    WAIT5U                  ; 2µs to process
        RETLW 0                         ; 2µs to process

OW_RESET:
        OW_HIZ                          ; Start with the line high
        CLRF    PDBYTE                  ; Clear the PD byte
        OW_LO
        WAIT    .500                    ; Drive Low for 500µs
        OW_HIZ
        WAIT    .70                     ; Release line and wait 70µs for PD Pulse
        BTFSS   PORTB,DQ                ; Read for a PD Pulse
        INCF    PDBYTE,F                ; Set PDBYTE to 1 if get a PD Pulse
        WAIT    .430                    ; Wait 430µs after PD Pulse
        RETLW   0

DSTXBYTE:                               ; Byte to send starts in W
        MOVWF   IOBYTE                  ; We send it from IOBYTE
        MOVLW   .8
        MOVWF   COUNT                   ; Set COUNT equal to 8 to count the bits
DSTXLP:
        OW_LO
        NOP
        NOP
        NOP                             ; Drive the line low for 3µs
        RRCF    IOBYTE,F
        BTFSC   STATUS,C                ; Check the LSB of IOBYTE for 1 or 0
        BSF     TRISB,DQ                ; HiZ the line  if LSB is 1
        WAIT    .60                     ; Continue driving line for 60µs
        OW_HIZ                          ; Release the line for pullup
        NOP
        NOP                             ; Recovery time of 2µs
        DECFSZ  COUNT,F                 ; Decrement the bit counter
        GOTO    DSTXLP
        RETLW   0

DSRXBYTE:                               ; Byte read is stored in IOBYTE
        MOVLW   .8
        MOVWF   COUNT                   ; Set COUNT equal to 8 to count the bits
DSRXLP:
        OW_LO
        NOP
        NOP
        NOP
        NOP
        NOP
        NOP                             ; Bring DQ low for 6µs
        OW_HIZ
        NOP
        NOP
        NOP
        NOP                             ; Change to HiZ and Wait 4µs
        MOVF    PORTB,W                 ; Read DQ
        ANDLW   1<<DQ                   ; Mask off the DQ bit
        ADDLW   .255                    ; C = 1 if DQ = 1: C = 0 if DQ = 0
        RRCF    IOBYTE,F                ; Shift C into IOBYTE
        WAIT    .50                     ; Wait 50µs to end of time slot
        DECFSZ  COUNT,F                 ; Decrement the bit counter
        GOTO    DSRXLP
        RETLW   0

; ---------------------------------------------------------------------------
; Strings used in the program
; ---------------------------------------------------------------------------
jan     db " - Jan Wagemakers - ",0
aan     db "___",0
uit     db "_/_",0
spc20	db "                    ",0
auto	db "AUTO ", 0
man	db " MAN ", 0
hPa	db "hPa ",0
; ---------------------------------------------------------------------------
; END
; ---------------------------------------------------------------------------
        END

