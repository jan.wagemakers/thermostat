2023.08.19 : Note, I don't use this setup anymore. 
Instead, I switched to a [Raspberry Pico W](https://www.raspberrypi.com/documentation/microcontrollers/raspberry-pi-pico.html).
This new setup is not documented yet...

# Thermostat

This is a personal project where I'm creating a *smart* thermostat. The idea
is to make a *dumb* thermostat where you can change the setpoint over
**bluetooth**. A small computer like the Raspberry Pi communicates with this
*dumb* thermostat over bluetooth to make the *dumb* thermostat *smart*.

Theoretically it is possible to let just a computer like the Raspberry Pi
control the heating, but I don't trust computers enough to let them do the
control directly. Think about things like sd-card or file system corruption,
which on occasion can happen. 

By making use of a *dumb* microcontroller based thermostat, it's still
possible to control the heating when the *smart* part fails.

## 1. pic18f1320 : dumb thermostat

* [schematic diagram](https://gitlab.com/jan.wagemakers/thermostat/blob/master/pic18f1320/thermostat.png)
* software, written in assembler (gpasm - GNU PIC assembler)

### 1.1. Configuration of the HC-05 Bluetooth module.

The pic18f1320 communicates with a HC-05 Bluetooth module at a speed of 2400
baud. To configure the HC-05:
* connect it to the serial port of a Raspberry Pi (/dev/ttyS0) 
* put the HC-05 in "AT mode" by pressing it's button. 
* In a terminal program like [minicom](https://en.wikipedia.org/wiki/Minicom) enter the command `AT+UART=2400,0,0`.

It's also a good idea to change the default password of the HC-05 by
entering the command `AT+PSWD:4321`. You can get the current password with
`AT+PSWD?` 

### 1.2. Commands

By using a HC-05 Bluetooth module connected to the pic18f1320
microcontroller it's possile to send commands to the thermostat.

The following commands are recognized by the thermostat:

<table style="overflow:hidden; width:auto;">
  <thead>
    <tr>
      <th> </th>
      <th>Command to µC</th>
      <th>Answer from µC</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Get current temperature (Process Value)</td>
      <td><code class="highlighter-rouge">PV?</code></td>
      <td><code class="highlighter-rouge">21.0</code></td>
    </tr>
    <tr>
      <td>Get current SetPoint</td>
      <td><code class="highlighter-rouge">SP?</code></td>
      <td><code class="highlighter-rouge">20.7</code></td>
    </tr>
    <tr>
      <td>Set SetPoint</td>
      <td><code class="highlighter-rouge">SP=20.6</code></td>
      <td><code class="highlighter-rouge">20.6</code></td>
    </tr>
    <tr>
      <td>Get OutPut . mode </td>
      <td><code class="highlighter-rouge">OP?</code></td>
      <td>
	      <table style="overflow:hidden; width:auto;">
		      <thead>
			      	<tr>
				      <th> </th>
				      <th>Relay</th>
				      <th>Mode</th>
			      	</tr>
		      </thead>
		      <tbody>     
 	      			<tr>
		      			<td><code class="highlighter-rouge">0.0</code></td>
		      			<td>OFF</td>
		      			<td>MAN</td>
	      			</tr>
				<tr>
		      			<td><code class="highlighter-rouge">1.0</code></td>
		      			<td>ON</td>
		      			<td>MAN</td>
	      			</tr>
	      			<tr>
		      			<td><code class="highlighter-rouge">0.1</code></td>
		      			<td>OFF</td>
		      			<td>AUTO</td>
	      			</tr>
	      			<tr>
		      			<td><code class="highlighter-rouge">1.1</code></td>
		      			<td>ON</td>
		      			<td>AUTO</td>
	      			</tr>
		      </tbody>
	</table>
      </td>
    </tr>
    <tr>
      <td>Set Outside Temperature</td>
      <td><code class="highlighter-rouge">OT=-1.6</code></td>
      <td><code class="highlighter-rouge">-1.6</code></td>
    </tr>
    <tr>
      <td>Set (Outside) Atmospheric pressure</td>
      <td><code class="highlighter-rouge">OA=0999</code></td>
      <td><code class="highlighter-rouge">0999</code></td>
    </tr>
    <tr>
      <td>Set Year</td>
      <td><code class="highlighter-rouge">YY=19</code></td>
      <td><code class="highlighter-rouge">19</code></td>
    </tr>
    <tr>
      <td>Set Month</td>
      <td><code class="highlighter-rouge">MM=03</code></td>
      <td><code class="highlighter-rouge">03</code></td>
    </tr>
    <tr>
      <td>Set Day</td>
      <td><code class="highlighter-rouge">DD=09</code></td>
      <td><code class="highlighter-rouge">09</code></td>
    </tr>
    <tr>
      <td>Set hour</td>
      <td><code class="highlighter-rouge">hh=10</code></td>
      <td><code class="highlighter-rouge">10</code></td>
    </tr>
    <tr>
      <td>Set minutes</td>
      <td><code class="highlighter-rouge">mm=58</code></td>
      <td><code class="highlighter-rouge">58</code></td>
    </tr>
    <tr>
      <td>unknown</td>
      <td><code class="highlighter-rouge">blabla</code></td>
      <td><code class="highlighter-rouge">?</code></td>
    </tr>
  </tbody>
</table>

### 1.3. Pictures PCB

> ![pcb1](http://www.janwagemakers.be/jekyll/assets/images/2018/PCB1.png)

> ![pcb2](http://www.janwagemakers.be/jekyll/assets/images/2018/PCB2.png)

> ![pcb3](http://www.janwagemakers.be/jekyll/assets/images/2018/PCB3.png)

> ![pcb4](http://www.janwagemakers.be/jekyll/assets/images/2018/PCB4.png)

> ![pcb5](http://www.janwagemakers.be/jekyll/assets/images/2018/PCB5.png)

> ![case](http://www.janwagemakers.be/jekyll/assets/images/2019/CASE.png)

> ![case new LCD](http://www.janwagemakers.be/jekyll/assets/images/2019/CASEnewLCD.png)

## 2. Raspberry Pi : smart thermostat

JAVA Spring Boot program that:

* communicates with *dumb* thermostat over bluetooth
* checks if a user is home by checking smartphone/bluetooth or smartphone/IP-address
* creates a web interface at port 8080
* reads the [outside temperature and atmospheric pressure](http://www.janwagemakers.be/jekyll/java/2019/04/06/metar.html) from [https://tgftp.nws.noaa.gov/data/observations/metar/stations/](https://tgftp.nws.noaa.gov/data/observations/metar/stations/)  
* use the outside temperature and atmospheric pressure to lower the setpoint if it's warm outside
* it's possible to add/delete/change "users","rules" and "configuration" by using the web interface 

### 2.1. Configure Bluetooth

On a terminal enter the command `bluetoothctl` and then enter:

``` 
power on
agent on
scan on
pair xx:xx:xx:xx:xx:xx
connect xx:xx:xx:xx:xx:xx
trust xx:xx:xx:xx:xx:xx
```

### 2.2. Configure the RFCOMM *serial port*

The communication over bluetooth works by the RFCOMM protocol. To set up
the RFCOMM *serial port* enter the command `rfcomm bind 0 xx:xx:xx:xx:xx:xx`.
Because after a reboot, you have to issue this command again, it's a good
idea to include the following line in `/etc/crontab`:

```
@reboot         root    /usr/bin/rfcomm bind 0 xx:xx:xx:xx:xx:xx
``` 

To test the communication, use the command `minicom -D /dev/rfcomm0`

### 2.3. hcitool : detect smartphone 

To detect a smartphone by Bluetooth (to check if somebody is home), hcitool
is used. To be able to run hcitool without being root, enter the command:
``` 
sudo setcap 'cap_net_raw,cap_net_admin+eip' `which hcitool`
```

### 2.4. systemd : start thermostat at boot

Create a file `/etc/systemd/system/thermostat.service` with the following
content:

```
# /etc/systemd/system/thermostat.service

[Unit]
Description=thermostat
After=syslog.target

[Service]
User=your_username
Restart=always
RestartSec=3
ExecStart=/usr/bin/java -jar /home/your_username/thermostat.jar
StandardOutput=journal
StandardError=journal
SyslogIdentifier=thermostat

[Install]
WantedBy=multi-user.target
```

Then, in a terminal enter the following commands:

```
systemctl start thermostat.service
systemctl enable thermostat.service
```

## 3. Source

[https://gitlab.com/jan.wagemakers/thermostat/](https://gitlab.com/jan.wagemakers/thermostat/)

## 4. Demo

A demo version of the web interface is available at 
[http://janw.mooo.com:24388](http://janw.mooo.com:24388). 

Note that in this demo version it's not possible to change the database
("rules", "users", "configuration"). The current temperature it shows is
just a random value. It's just there to show the current
interface.

## 5. Screenshots

### 5.1. Index

> ![index](http://www.janwagemakers.be/jekyll/assets/images/2018/thermostat_index.png)

### 5.2. Users

> ![users](http://www.janwagemakers.be/jekyll//assets/images/2018/thermostat_users.png)

### 5.3. Rules

> ![rules](http://www.janwagemakers.be/jekyll//assets/images/2018/thermostat_rules.png)

### 5.4. Configuration

> ![configuration](http://www.janwagemakers.be/jekyll//assets/images/2019/thermostat_configuration.png)

### 5.5. Log

> ![log](http://www.janwagemakers.be/jekyll//assets/images/2018/thermostat_log.png)
